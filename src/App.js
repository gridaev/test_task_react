import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import React from "react";
import axios from "axios";  // типа ajax
import { BsSortUpAlt, BsSortUp } from 'react-icons/bs'; // импорт компонентов двух иконок
import { BiEdit, BiSave } from 'react-icons/bi';
import { RiArrowGoBackFill } from 'react-icons/ri';
import { RiDeleteBin5Line } from 'react-icons/ri';

const format = require('date-format') // импорт

function App() {  // типа переменные
    const [tasks, setTasks] = React.useState([])
    const [sort, setSort] = React.useState('desc')
    const [error, setError] = React.useState(null)
    const [editError, setEditError] = React.useState(false)
    const [input, setInput] = React.useState('')
    const [editInput, setEditInput] = React.useState('')
    const [editTaskId, setEditTaskId] = React.useState(null)
    const [delTaskId, setDelTaskId] = React.useState(null)

    React.useEffect(() => { // иницилизация
        collectionTasks()
    }, [])

    const collectionTasks = (sorting = "desc") => {
        axios.get(`http://localhost/_get-tasks.html?sort=${sorting}`)
            .then(res => {
                console.log(res.data)
                setTasks(res.data) // записываю ответ от сервера в переменную tasks
            })
    }

    const updateInputValue = e => {
        console.log(e.target.value)
        setInput(e.target.value) // обновляем значение переменной input
        setError(null) // оновляем переменную error
    }

    const updateEditInputValue = e => {
        console.log(e.target.value)
        setEditInput(e.target.value) // обновляем значение переменной input
        setEditError(null) // оновляем переменную error
    }

    const toggleSort = () => { // сортровка
        let sorting = "asc"
        if(sort === "desc") {
            setSort("asc")
        }else{
            sorting = "desc"
            setSort("desc")
        }
        collectionTasks(sorting)
    }

    const addTask = () => {  // добаить
        if(input === "") {
            setError("Поле обязательно к заполнению!")
        }else{
            axios.post(
                `http://localhost/_add-task.html`,
                JSON.stringify({     // преоброзую с str
                    "name": input
                }),
                {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            )
                .then(res => {
                    collectionTasks(sort)
                    setInput('') // чистим поле input
                })
        }
    }

    const saveTask = () => {
        if(editInput === "") {
            setEditError(true)
        }else{
            axios.post(
                `http://localhost/_save-task.html`,
                JSON.stringify({     // преоброзую с str
                    "id": editTaskId,
                    "name": editInput
                }),
                {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            )
                .then(res => {
                    collectionTasks(sort)
                    btnCancelEditTask() // чистим поле input
                })
        }
    }

    const delTask = (taskId) => {
        axios.post(
            `http://localhost/_del-task.html`,
            JSON.stringify({
                "id": taskId
            }),
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
            .then(res => {
                collectionTasks(sort)
            })
    }

    const btnCancelEditTask = () => {
        setEditTaskId('')
        setEditInput('')
        setEditError(null)
    }

    const btnEditTask = task => {
        setEditTaskId(task.id)
        setEditInput(task.name)
        setEditError(null)
    }

    return (
        <div className='container'>
            <h1>Список дел</h1>
            <div className="mb-3 mt-3 col-lg-3">
                <input
                    type="text"
                    className={`form-control ${error !== null ? 'is-invalid' : ''}`}  // типа f-строка, условие на класс в бутстрапе
                    placeholder="Введите название"
                    value={input}
                    onChange={updateInputValue} // обновляю переменную input
                />
                <div className="invalid-feedback">
                    {error}
                </div>
            </div>
            <button className="btn btn-primary" onClick={addTask}>Добавить</button>
            <table className="table mt-5">
                <thead>
                <tr>
                    <th scope="col">Название</th>
                    <th scope="col">
                        <div className="d-flex align-items-center">
                            Время создания
                            <button className="button-sort" onClick={toggleSort}>
                                {sort === "desc" ? <BsSortUpAlt/> : <BsSortUp/> /* меняем иконку сорт */}
                            </button>
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody>
                {
                    tasks.map(task => {  // типа цикл
                        return (
                            <tr key={task.id}>
                                <td>
                                    <div className="d-flex align-items-center">
                                        <button className="button-edit" onClick={() => delTask(task.id)}>
                                            <RiDeleteBin5Line/>
                                        </button>
                                        {editTaskId === task.id ? <>
                                        <button className="button-edit" onClick={btnCancelEditTask}>
                                            <RiArrowGoBackFill/>
                                        </button>
                                        <button className="button-edit" onClick={saveTask}>
                                            <BiSave/>
                                        </button>
                                        <input
                                            type="text"
                                            className={`form-control ${editError === true ? 'is-invalid' : ''}`}
                                            placeholder="Введите название"
                                            onChange={updateEditInputValue} // обновляю переменную input
                                            value={editInput}
                                        />
                                            </>
                                            : <>
                                            <button className="button-edit" onClick={() => btnEditTask(task)}>
                                                <BiEdit/>
                                            </button>
                                                {task.name}
                                        </>
                                        }
                                    </div>
                                </td>
                                <td>
                                    {format.asString("dd.MM.yyyy hh:mm:ss", new Date(task.created_at))}
                                </td>
                            </tr>
                        )
                    })
                }
                </tbody>
            </table>
        </div>
    );
}

export default App;
